/**
 * Display.model.ts
 * Chart display model for common calculations.j
 */
'use strict';
import * as _ from 'lodash';
//import { Bar } from './Bar.interface';

/**
 * Margin interface.
 */
export interface Margin {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

/**
 * DisplayModel class.
 * @class DisplayModel
 */
export class DisplayModel {

  /**
   * Method to calculate uniformed margins
   * @method
   *
   * @return Margin
   */
  public static getMargins(): Margin {
    return {
      top: 20,
      right: 50,
      bottom: 20,
      left: 20
    };
  }

  /**
   * Returns the screen width of the user's device.
   * @method getScreenWidth
   *
   * @return number
   */
  public static getScreenWidth(): number {
    return window.innerWidth;
  }

  /**
   * Build Y Axis tick interval values.
   * @method
   *
   * @param { number } minValue - the minimum tick value.
   * @param { number } maxValue - the maximum tick value.
   * @param { number } tickSize - the size of the ticks.
   *
   */
  public static getYAxisTickIntervals(
    minValue: number, 
    maxValue: number, 
    tickSize: number
  ): Array<number> {
    // Get the lowest and highest tick value.
    const lowTickValue: number = minValue * tickSize,
          highTickValue: number = maxValue * tickSize;

    // Set the current tick value to the lowest price.
    let currentTickValue: number = lowTickValue,
        // Declare an array to hold the y axis tick values.
        yTickValues: Array<number> = [];

    // Loop through the tick values, starting with the lowest value and ending
    // when we reach the highest value.
    while (currentTickValue <= highTickValue) {
      // Add the current tick value to the y tick values array.
      yTickValues.push(currentTickValue);

      // Increment the current tick value by the tick size.
      currentTickValue = currentTickValue + tickSize;
    } 

    // Return the yTickValues array.
    return yTickValues;
  }

  /**
   * Retrieve the y axis display values.
   * @method getYAxisDisplayValues
   *
   * @param { number } minValue - the minimum tick price.
   * @param { number } maxValue - the maximum tick price.
   * @param { number } tickSize - the dollar size of each tick.
   * @param { number } height - the height of the chart element.
   *
   * @return Array<number>
   */
  public static getYAxisDisplayValues(
    minValue: number, 
    maxValue: number, 
    tickSize: number,
    chartHeight: number
  ): Array<number> {

    // Get the lowest and highest tick value.
    const numberOfTicks: number = maxValue - minValue;

    // Define the maximum number of ticks to display based on a maximum of 1
    // price per 50 pixels.
    let maxDisplayTicks: number = Math.floor(chartHeight / 75),
        // Declare an array to hold the y axis tick values.
        yTickValues: Array<number> = [],
        // Define the grouping of the tick values to be stored in the array.
        tickGrouping: number = 1;

    // Calculate the ticks to be shown.
    maxDisplayTicks = (maxDisplayTicks >= numberOfTicks) ? numberOfTicks - 1 : maxDisplayTicks; 

    maxDisplayTicks = maxDisplayTicks;

    // Set a tick interval to be used for identifying which tick values should
    // appear in the array.
    const tickInterval = Math.floor(numberOfTicks / maxDisplayTicks) * tickSize;

    // If the interval is greater than a single point.
    if (tickInterval >= 1) {
      // Set the tickGrouping to tick interval divided by the tick size. 
      tickGrouping = Math.floor(tickInterval) / tickSize; 
    }

    if (tickInterval >= 0.5 && tickInterval < 1) {
      // Set the tickGrouping to tick interval divided by the tick size. 
      tickGrouping = 0.5 / tickSize;  
    }

    // Loop through each tick.
    for (let i = minValue; i <= maxValue; i++) {

      // If this is a whole number, it means we have a registerable value to be
      // added to the array.
      if (Number.isInteger(i / tickGrouping)) {

        // Add the current tick value to the y tick values array.
        yTickValues.push(i * tickSize);
      }

    }

    // Create a maximum value if we don't have an existing one.
    if (_.indexOf(yTickValues, maxValue * tickSize) < 0) {
      yTickValues.push(maxValue * tickSize);
    }

    // If the maximum value and the second highest value are smaller than the
    // accepted tick interval, remove the second highest value.
    if ((yTickValues[yTickValues.length - 1] - yTickValues[yTickValues.length - 2]) < tickInterval) {
      _.pull(yTickValues, yTickValues[yTickValues.length - 2]);
    }

    // Return the yTickValues array.
    return yTickValues;
  }
}
