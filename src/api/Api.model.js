/**
 * api.ts
 * API Model Class.
 */
'use strict';
exports.__esModule = true;

/**
 * API Class.
 * @class APIModel
 */
var APIModel = /** @class */ (function () {
    function APIModel() {
    }
    /**
     * Static method to return the server API path.
     */
    APIModel.getServerPath = function () {
        return 'http://node_server/api/';
    };
    /**
     * Converts a response to JSON data.
     * @method
     *
     * @param { Response } response - the response object.
     *
     * @return JSON
     */
    APIModel.json = function (response) {
        // Return the response as a JSON.
        return response.json();
    };
    /**
     * Determines what type of status that should be returned.
     * @method
     *
     * @param { Response } response - the response object.
     *
     * @return Promise
     */
    APIModel.verifyStatus = function (response) {
        // Return a promise or rejection based on the status of the
        // response.
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response);
        }
        else {
            return Promise.reject(new Error(response.statusText));
        }
    };
    /**
     * Performs a request to the server api.
     */
    APIModel.request = function (path, data) {
        // Perform the fetch of the data.
        return fetch(APIModel.getServerPath() + path, data)
            .then(APIModel.verifyStatus)
            .then(APIModel.json);
    };
    return APIModel;
}());
exports.APIModel = APIModel;
