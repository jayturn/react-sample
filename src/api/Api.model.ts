/**
 * api.ts
 * API Model Class.
 */
'use strict';

/**
 * Interface for the responses.
 */
export interface ResponseInterface {
  body: Array<{}>;
}

/**
 * API Class.
 * @class APIModel
 */
export class APIModel {

  /**
   * Static method to return the server API path.
   *
   * @return string
   */
  public static getServerPath(): string {
    // Simply pointing to the data directory for the purposes of the code sample.
    return './data/';
  }

  /**
   * Converts a response to JSON data.
   * @method
   *
   * @param { Response } response - the response object. 
   *
   * @return JSON
   */
  public static json(response: Response): Promise<ResponseInterface> {
    // Return the response as a JSON.
    return response.json();
  }

  /**
   * Determines what type of status that should be returned.
   * @method
   *
   * @param { Response } response - the response object. 
   *
   * @return Promise
   */
  public static verifyStatus(response: Response): Promise<Response> {
    // Return a promise or rejection based on the status of the
    // response.
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(new Error(response.statusText));
    }
  }

  /**
   * Performs a request to the server api.
   */
  public static request(path: string, data: {}): Promise<ResponseInterface> {

    // Perform the fetch of the data.
    return fetch(APIModel.getServerPath() + path, data)
      .then(APIModel.verifyStatus)
      .then(APIModel.json);
  }
}
