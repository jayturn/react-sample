/**
 * ChartState.interface
 */
'use strict';

import { Bar } from './Bar.interface';
import { ChartParameters } from './ChartParameters.interface';

export interface ChartState {
  bars: Array<Bar>;
  parameters: ChartParameters;
}
