/**
 * Chart.tsx
 */
'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var Api_model_1 = require("../api/Api.model");
var ReactFauxDom = require("react-faux-dom");
var D3 = require("d3");
/**
 * Chart class.
 * @class
 */
var Chart = /** @class */ (function (_super) {
    __extends(Chart, _super);
    /**
     * Class constructor.
     * @constructor
     */
    function Chart(props) {
        var _this = 
        // Invoke the parent constructor.
        _super.call(this, props) || this;
        // Set the default state to the provided properties.
        _this.state = {
            bars: []
        };
        return _this;
    }
    /**
     * Mounted component lifecycle hook.
     */
    Chart.prototype.componentDidMount = function () {
        var _this = this;
        // Define the chart code.
        var chartCode = this.props.instrument + this.props.chartType + this.props.scale;
        // Retrieve the chart data and set the state.
        Api_model_1.APIModel.request('chart/' + chartCode, {})
            .then(function (response) {
            if (!response.body || response.body.length === 0) {
                return;
            }
            var results = response.body;
            _this.setState({ bars: results });
        })["catch"](function (error) {
            console.log(error, 'ERROR');
        });
    };
    /**
     * Render method.
     *
     * @return HTMLElement
     */
    Chart.prototype.render = function () {
        // Get the chart data.
        var data = this.state.bars;
        // Set margin.
        var margin = {
            top: 20,
            right: 20,
            bottom: 20,
            left: 20
        };
        var width = 600 - margin.left - margin.right;
        var height = 600 - margin.top - margin.bottom;
        var x = D3.scaleBand()
            .rangeRound([0, width]);
        var y = D3.scaleLinear()
            .range([height, 0]);
        var xAxis = D3.axisBottom()
            .scale(x);
        var yAxis = D3.axisLeft()
            .scale(y)
            .ticks(10, '%');
        // Create the React Faux element.
        var el = new ReactFauxDom.Element('div');
        var svg = D3.select(div).append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', "translate(" + margin.left + ", " + margin.top + ")");
        x.domain(data.map(function (d) { return new Date(d.lastTransactionDate); }));
        y.domain([D3.min(data, function (d) { return d.low; }), D3.max(data, function (d) { return d.high; })]);
        svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', "translate(0, " + height + ")")
            .call(xAxis);
        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis)
            .append('text')
            .attr('transform', 'rotate(-90)')
            .attr('y', 6)
            .attr('dy', '.71em')
            .style('text-anchor', 'end')
            .text('Price');
        svg.selectAll('.bar')
            .data(data)
            .enter().append('rect')
            .attr('class', 'bar')
            .attr('x', function (d) { return x(d.lastTransactionDate); })
            .attr('width', 20)
            .attr('y', function (d) { return y(d.high); })
            .attr('height', function (d) {
            return height - y(d.high);
        });
        // Set the reference on our new element.
        //el.setAttribute('ref', 'chart');
        // Create the SVG element for data rendering.
        //let svg = D3.select(el).append('svg')
        //.attr('width', 600)
        //.attr('height', 400);
        //svg.data(this.state.bars)
        //.enter().append('div')
        // Set the renderable variables based on the class properties.
        //const { chartType, scale }: Props = this.props;
        // Return the chart element.
        return el.toReact();
    };
    /**
     * Retrieves the requested chart data.
     */
    Chart.prototype.getChartData = function () {
        // Perform a fetch using the provided properties and update state.
        // Set the values of the chart.
        //if (this.state.chartType === 'tick') {
        //this.setState({
        //chartType: 'renko',
        //scale: '10-15-5'
        //});
        //} else {
        //this.setState({
        //chartType: 'tick',
        //scale: '500'
        //});
        //}
    };
    return Chart;
}(React.Component));
exports["default"] = Chart;
