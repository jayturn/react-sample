/**
 * ChartParameters.interface
 */
'use strict';

export interface ChartParameters {
  width: number;
  height: number;
}
