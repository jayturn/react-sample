/**
 * Bar.interface.ts
 */
'use strict';

export interface Bar {
  _id: string;
  instrument: string;
  year: number;
  period: number;
  chartCode: string;
  open: number;
  high: number;
  low: number;
  close: number;
  ticks: number;
  volume: number;
  askVolume: number;
  bidVolume: number;
  firstRawId: string;
  lastRawId: string;
  lastTransactionDate: string;
  firstTransactionDate: string;
  bar: Array<PriceVolume>;
  cumulativeDelta: {
    open: number;
    high: number;
    low: number;
    close: number;
  };
}

export interface PriceVolume {
  price: number;
  volume: number;
  askVolume: number;
  bidVolume: number;
  ticks: number;
}
