/**
 * Chart.tsx
 */
'use strict';

// Import the dependent modules.
import * as React from 'react';
import * as ReactFauxDom from 'react-faux-dom';

// Import the dependent models.
import { APIModel, ResponseInterface } from '../api/Api.model';
import './Chart.css';
import { CandlestickChartModel as CandlestickChart } from './candlestick/CandlestickChart.model';
import { RenkoChartModel as RenkoChart } from './renko/RenkoChart.model';

// Import the dependent interfaces.
import { Bar } from './Bar.interface';
import { ChartOptions } from './ChartOptions.interface';
import { ChartState } from './ChartState.interface';

/**
 * Chart component class.
 * @class
 */
export default class Chart extends React.Component<ChartOptions, ChartState> {
  // Declare the chart element to be referenced.
  private chartElement: HTMLDivElement | null;

  /**
   * Initialise the Chart class with an empty state.
   * @constructor
   *
   * @param { ChartOptions } props - chart options.
   */
  constructor(props: ChartOptions) {
    // Invoke the parent component constructor.
    super(props);

    // Define the default state properties.
    this.state = { 
      bars: [],
      parameters: {
        width: 0,
        height: 0
      }
    };
  }

  /**
   * Perform an API request for chart data after the component is mounted.
   * 
   */
  public componentDidMount() {
    // Assign the chart code to be used for API data requests.
    const chartCode = this.props.instrument + this.props.chartType + this.props.scale;

    // Retrieve the chart data from the API.
    APIModel.request('chart/' + chartCode, {})
      .then((response: ResponseInterface) => {

        // Exit if the API failed to return data.
        if (!response.body || response.body.length === 0) {
          return;
        }
        
        // Set the component state using the results from the API request.
        this.setState({ 
          bars: response.body as Array<Bar>,
          parameters: {
            height: (this.chartElement) ? this.chartElement.clientHeight : 0,
            width: (this.chartElement) ? this.chartElement.clientWidth : 0
          }
        });
      })
      .catch((error: Error) => {
        /** 
         * @todo Log errors using winston.
         */
        console.log(error, 'ERROR');
      });
  }

  /**
   * Render the chart component.
   *
   * @return HTMLElement
   */
  public render() {
    // Define a faux dom element to be used for delayed chart creation.
    let chart: ReactFauxDom.Element,
        options: ChartOptions = this.props;

    // Create the relevant chart based on the provided component property.
    switch (this.props.displayType) {
      case 'candlestick':
        chart = CandlestickChart.create(this.state.bars, this.state.parameters, options);
        break;
      case 'renko':
        chart = RenkoChart.create(this.state.bars, this.state.parameters, options);
        break;
      default:
        chart = CandlestickChart.create(this.state.bars, this.state.parameters, options);
    }

    // Return the chart component referencing the faux dom element to support
    // D3 rendering.
    return(
      <div className="chart" ref={(chartElement: HTMLDivElement | null) => this.chartElement = chartElement}> 
        {chart.toReact()}
      </div>
    );
  }
}
