/**
 * CandlestickChart.ts
 * Candlestick chart model.
 */
'use strict';

// Import the dependent modules.
import * as _ from 'lodash';
import * as d3 from 'd3';
import * as ReactFauxDom from 'react-faux-dom';

// Import the dependent models.
import { CumulativeDeltaModel as CumulativeDelta } from '../../indicators/cumulativeDelta/CumulativeDelta.model';
import { DisplayModel as Display, Margin } from '../../display/Display.model';

// Import the dependent interfaces.
import { Bar } from '../Bar.interface';
import { ChartOptions } from '../ChartOptions.interface';
import { ChartParameters } from '../ChartParameters.interface';

/**
 * CandlestickChart Class.
 * @class CandlestickChart
 */
export class CandlestickChartModel {

  /**
   * Static method to create the candlestick chart.
   * @method create
   *
   * @param { Array<Bar> } bars - the array of bars. 
   * @param { ChartParameters } parameters - the chart parameters. 
   * @param { ChartOptions } settings - the chart settings. 
   *
   * @return ReactFauxDom.Element
   */
  public static create(bars: Array<Bar>, parameters: ChartParameters, settings: ChartOptions): ReactFauxDom.Element {

    // Reverse the bars so they are presented from the first transaction on the
    // left to the last on the right.
    _.reverse(bars);
    
    // Define the default chart values.
    const margin: Margin = Display.getMargins(),
          tickSize: number = parseFloat(settings.tickSize),
          // Set the highest and lowest prices for the bars.
          highestPrice: number = (d3.max(bars.map((bar) => {
            return Math.max(bar.open, bar.high, bar.low, bar.close);
          })) || 0),
          lowestPrice: number = (d3.min(bars.map((bar) => {
            return Math.min(bar.open, bar.high, bar.low, bar.close);
          })) || 0),
          // Define the yAxis tick values.
          yAxisTickValues = Display.getYAxisDisplayValues(lowestPrice, highestPrice, tickSize, parameters.height),
          // Set the height and width for the chart.
          width: number = Display.getScreenWidth() - margin.left - margin.right,
          height: number = 0.5 * width,
          chartTitle: string = settings.title;
      
    // Create a linear scale to be used for the xAxis.
    const x = d3.scaleLinear()
      .domain([0, bars.length])
      .range([0, width]);

    // Create a linear scale to be used for the yAxis.
    const y = d3.scaleLinear()
      .domain([lowestPrice * tickSize, highestPrice * tickSize])
      .range([height, 0]);

    // Set the xAxis to the bottom of the chart.
    const xAxis = d3.axisBottom(x);

    // Set the yAxis to the right of the chart.
    const yAxis = d3.axisRight(y);

    // Set the tick values for the yAxis.
    yAxis.tickValues(yAxisTickValues);
      
    // Set the tick format.
    yAxis.tickFormat(d3.format('.2f'));

    // Set the number of ticks to be shown for the xAxis.
    xAxis.ticks(0);

    // Create the React Faux element.
    let el: ReactFauxDom.Element = new ReactFauxDom.Element('div');

    // Provide D3 with the faux dom element so it creates the SVG element
    // whilst maintaining the React dom tree.
    let svg = d3.select(el)
      .classed('svg-container', true)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('viewBox', `0 0 ${width + (margin.left * 5)} ${height + (margin.top * 2.2)} `)
      .classed('svg-content-responsive candlestick', true)
      .append('g')
      .attr('transform', `translate(${margin.left / 2}, ${0 + (margin.top * 2)})`);

    svg.append('g')
      .classed('right-border', true)
      .append('svg:line')
      .attr('x1', width + 1)
      .attr('x2', width + 1)
      .attr('y1', 0)
      .attr('y2', height + (margin.top * 2.2));

    // Create the cumulative delta svg element.
    el = CumulativeDelta.create(bars, parameters, el);

    // Create the y-axis grid lines.
    svg.append('g')
      .classed('y-grids', true)
      .selectAll('.y-grid')
      .data(() => {
        // Return the y axis values.
        return yAxisTickValues;
      })
      .enter().append('svg:line')
      .attr('class', 'y-grid')
      .attr('y1', (d) => {
        return y(d) + 0.5;
      })
      .attr('x1', width)
      .attr('y2', (d) => {
        return y(d) + 0.5;
      })
      .attr('x2', 0);

    // Create the y-axis.
    svg.append('g')
      .attr('class', 'y-axis')
      .attr('transform', `translate(${width}, 0)`)
      .call(yAxis);

    // Create the bar tails.
    svg.selectAll('.tails')
      .data(bars)
      .enter()
      .append('line')
      .attr('class', (bar: Bar) => {
        // Set the classes based on the bar direction.
        return (bar.open > bar.close) ? 'tails down' : 'tails up';
      })
      .attr('x1', (bar: Bar, i: number) => {
        return x(i) + 5;
      }) 
      .attr('x2', (bar: Bar, i: number) => {
        return x(i) + 5;
      }) 
      .attr('y1', (bar: Bar) => {
        return y(bar.high * tickSize);
      })
      .attr('y2', (bar: Bar) => {
        return y(bar.low * tickSize);
      });

    // Create the bar body.
    svg.selectAll('.bar')
      .data(bars)
      .enter()
      .append('rect')
      .attr('class', (bar: Bar) => {
        // Set the classes based on the bar direction.
        return (bar.open > bar.close) ? 'bar down' : 'bar up';
      })
      .attr('x', (d, i) => {
        return x(i);
      })
      .attr('width', () => {
        return 10;
      })
      .attr('y', (bar, index) => {
        if (bar.open > bar.close) {
          return y(bar.open * tickSize);
        } else {
          return y(bar.close * tickSize);
        }
      })
      .attr('height', (bar, index) => { 
        if (bar.open > bar.close) {
          return y(bar.close * tickSize) - y(bar.open * tickSize); 
        } else {
          if (bar.open === bar.close) {
            return 2; 
          } else {
            return y(bar.open * tickSize) - y(bar.close * tickSize); 
          }
        }
      });
    
    // Add the chart title.
    svg.append('svg:text')
      .attr('class', 'chart-title')
      .attr('x', 0)
      .attr('y', -margin.top)
      .text(chartTitle);

    return el;
  }
}
