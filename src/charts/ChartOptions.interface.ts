/**
 * Chart.interface
 */
'use strict';

export interface ChartOptions {
  instrument: string;
  chartType: string;
  scale: string;
  displayType: string;
  title: string;
  tickSize: string;
}
