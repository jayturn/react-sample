/**
 * RenkoChart.ts
 * Renko chart model.
 */
'use strict';

import * as d3 from 'd3';
import * as ReactFauxDom from 'react-faux-dom';
import * as _ from 'lodash';
import { CumulativeDeltaModel as CumulativeDelta } from '../../indicators/cumulativeDelta/CumulativeDelta.model';
import { Bar } from '../Bar.interface';
import { ChartOptions } from '../ChartOptions.interface';
import { ChartParameters } from '../ChartParameters.interface';
import { DisplayModel as Display, Margin } from '../../display/Display.model';

/**
 * RenkoChart Class.
 * @class RenkoChart
 */
export class RenkoChartModel {

  /**
   * Static method to create the renko chart.
   * @method create
   *
   * @param { Array<Bar> } bars - the array of bars. 
   *
   * @return
   */
  public static create(bars: Array<Bar>, parameters: ChartParameters, settings: ChartOptions): ReactFauxDom.Element {

    // Reverse the bars so they are presented from the first transaction on the
    // left to the last on the right.
    _.reverse(bars);
    
    // Set chart values.
    const margin: Margin = Display.getMargins(),
          tickSize: number = 0.25,
          // Set the highest and lowest prices for the bars.
          highestPrice: number = (d3.max(bars.map((bar) => {
            return Math.max(bar.open, bar.high, bar.low, bar.close);
          })) || 0),
          lowestPrice: number = (d3.min(bars.map((bar) => {
            return Math.min(bar.open, bar.high, bar.low, bar.close);
          })) || 0),
          // Define the yAxis display values.
          yAxisDisplayValues = Display.getYAxisDisplayValues(lowestPrice, highestPrice, tickSize, parameters.height),
          // Set the height and width for the chart.
          width = 700 - margin.left - margin.right,
          height = 0.5 * width,
          chartTitle: string = settings.title + ' L: 1337.50';
      
    // Create a linear scale to be used for the xAxis.
    const x = d3.scaleLinear()
      .domain([0, bars.length])
      .range([0, width]);

    // Create a linear scale to be used for the yAxis.
    const y = d3.scaleLinear()
      .domain([lowestPrice * tickSize, highestPrice * tickSize])
      .range([height, 0]);

    // Set the xAxis to the bottom of the chart.
    const xAxis = d3.axisBottom(x);

    // Set the yAxis to the right of the chart.
    const yAxis = d3.axisRight(y);

    // Set the tick values for the yAxis.
    yAxis.tickValues(yAxisDisplayValues);
      
    // Set the tick format.
    yAxis.tickFormat(d3.format('.2f'));

    // Set the number of ticks to be shown for the xAxis.
    xAxis.ticks(0);

    // Create the React Faux element.
    let el: ReactFauxDom.Element = new ReactFauxDom.Element('div');

    // Create the container to store the chart values.
    let svg = d3.select(el)
      .classed('svg-container', true)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('viewBox', `0 0 ${width + (margin.left * 5)} ${height + (margin.top * 2.2)} `)
      .classed('svg-content-responsive renko', true)
      .append('g')
      .attr('transform', `translate(${margin.left / 2}, ${0 + (margin.top * 2)})`);

    svg.append('g')
      .classed('right-border', true)
      .append('svg:line')
      .attr('x1', width + 1)
      .attr('x2', width + 1)
      .attr('y1', 0)
      .attr('y2', height + (margin.top * 2.2));

    // Create the cumulative delta svg element.
    el = CumulativeDelta.create(bars, parameters, el);

    // Create the y-axis grid lines.
    svg.append('g')
      .selectAll('.y-grids')
      .data(() => {
        // Return the y axis values.
        return yAxisDisplayValues;
      })
      .enter().append('svg:line')
      .attr('class', 'y-grids')
      .attr('y1', (d) => {
        return y(d) + 0.5;
      })
      .attr('x1', width)
      .attr('y2', (d) => {
        return y(d) + 0.5;
      })
      .attr('x2', 0);

    // Create the y-axis.
    svg.append('g')
      .attr('class', 'y-axis')
      .attr('transform', `translate(${width}, 0)`)
      .call(yAxis);

    // Create the bar tails.
    svg.selectAll('.tails')
      .data(bars)
      .enter()
      .append('line')
      .attr('class', (bar: Bar) => {
        return (bar.open > bar.close) ? 'tails down' : 'tails up';
      })
      .attr('x1', (bar: Bar, i: number) => {
        return x(i) + 5;
      }) 
      .attr('x2', (bar: Bar, i: number) => {
        return x(i) + 5;
      }) 
      .attr('y1', (bar: Bar) => {
        return y(bar.high * tickSize);
      })
      .attr('y2', (bar: Bar) => {
        return y(bar.low * tickSize);
      });

    // Create the bar body.
    svg.selectAll('.bar')
      .data(bars)
      .enter()
      .append('rect')
      .attr('class', (bar: Bar) => {
        return (bar.open > bar.close) ? 'bar down' : 'bar up';
      })
      .attr('x', (d, i) => {
        return x(i);
      })
      .attr('width', () => {
        return 10;
        //return xOrdinal.bandwidth();
        //return Math.round((width / bars.length) * 0.6);
      })
      .attr('y', (bar, index) => {
        if (bar.open > bar.close) {
          return y(bar.open * tickSize);
        } else {
          return y(bar.close * tickSize);
        }
      })
      .attr('height', (bar, index) => { 
        if (bar.open > bar.close) {
          return y(bar.close * tickSize) - y(bar.open * tickSize); 
        } else {
          if (bar.open === bar.close) {
            return 2; 
          } else {
            return y(bar.open * tickSize) - y(bar.close * tickSize); 
          }
        }
      });
    
    svg.append('svg:text')
      .attr('class', 'chart-title')
      .attr('x', 0)
      .attr('y', 0 - margin.top)
      .text(chartTitle);

    return el;
  }
}
