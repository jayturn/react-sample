/**
 * DeltaParameters.interface
 */
'use strict';

export interface DeltaParameters {
  width: number;
  height: number;
}
