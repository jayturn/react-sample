/**
 * Bar.interface.ts
 */
'use strict';

export interface DeltaBar {
  open: number;
  high: number;
  low: number;
  close: number;
  lastTransactionDate: string;
  lastRawId: string;
}
