/**
 * DeltaOptions.interface
 */
'use strict';

export interface DeltaOptions {
  instrument: string;
  chartType: string;
  scale: string;
  displayType: string;
  title: string;
}
