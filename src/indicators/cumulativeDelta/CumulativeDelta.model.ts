/**
 * CandlestickChart.ts
 * Candlestick chart model.
 */
'use strict';

import * as d3 from 'd3';
import * as ReactFauxDom from 'react-faux-dom';
//import * as _ from 'lodash';
import { Bar } from '../../charts/Bar.interface';
import { DeltaBar } from './DeltaBar.interface';
//import { DeltaOptions } from './DeltaOptions.interface';
import { ChartParameters } from '../../charts/ChartParameters.interface';
import { DisplayModel as Display, Margin } from '../../display/Display.model';

/**
 * CumulativeDelta Class.
 * @class CumulativeDelta
 */
export class CumulativeDeltaModel {

  /**
   * Static method to create the cumulative delta chart.
   * @method create
   *
   * @param { Array<Bar> } bars - the array of bars. 
   * @param { ChartParameters } parameters - the chart parameters object. 
   * @param { DeltaSettings } settings - the cumulative delta settings object. 
   *
   * @return
   */
  public static create(
    chartBars: Array<Bar>, 
    parameters: ChartParameters,
    el: ReactFauxDom.Element
  ): ReactFauxDom.Element {

    // Build the delta bars from the chart bars provided.
    const bars: Array<DeltaBar> = CumulativeDeltaModel.convertChartBarsToDeltaBars(chartBars);

    // Set chart values.
    const margin: Margin = Display.getMargins(),
          tickSize: number = 1,
          // Set the highest and lowest prices for the bars.
          highestPrice: number = (d3.max(bars.map((bar) => {
            return Math.max(bar.open, bar.high, bar.low, bar.close);
          })) || 0),
          lowestPrice: number = (d3.min(bars.map((bar) => {
            return Math.min(bar.open, bar.high, bar.low, bar.close);
          })) || 0),
          // Define the yAxis tick values.
          yAxisTickValues = Display.getYAxisDisplayValues(lowestPrice, highestPrice, tickSize, parameters.height),
          // Set the height and width for the chart.
          width = 700 - margin.left - margin.right,
          height = (0.2 * width);

    // Create a linear scale to be used for the xAxis.
    const x = d3.scaleLinear()
      .domain([0, bars.length])
      .range([0, width]);

    // Create a linear scale to be used for the yAxis.
    const y = d3.scaleLinear()
      .domain([lowestPrice * tickSize, highestPrice * tickSize])
      .range([height, 0]);

    // Set the xAxis to the bottom of the chart.
    const xAxis = d3.axisBottom(x);

    // Set the yAxis to the right of the chart.
    const yAxis = d3.axisRight(y);

    // Set the tick values for the yAxis.
    yAxis.tickValues(yAxisTickValues);
      
    // Set the tick format.
    yAxis.tickFormat(d3.format('.2f'));

    // Set the xAxis tick format using the last transaction date.
    xAxis.tickFormat((i: number) => {

      if (bars.length > 0) {

        if (bars[i]) {

          return d3.utcFormat('%H:%M:%S %d/%m/%y')(new Date(bars[i].lastTransactionDate));

        } else {

          return d3.utcFormat('%H:%M:%S %d/%m/%y')(new Date(bars[bars.length - 1].lastTransactionDate));

        }

      }

      return d3.timeFormat('%H:%M:%Y')(new Date());
    });

    // Set the number of ticks to be shown for the xAxis.
    xAxis.ticks(4);

    // Create the React Faux element.
    //let el: ReactFauxDom.Element = new ReactFauxDom.Element('div');

    // Create the container to store the chart values.
    let svg = d3.select(el)
      .classed('svg-container', true)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('viewBox', `0 0 ${width + (margin.left * 5)} ${height + (margin.top * 3.2)} `)
      .classed('svg-content-responsive candlestick', true)
      .append('g')
      .attr('transform', `translate(${margin.left / 2}, ${0 + (margin.top * 2)})`);

    svg.append('g')
      .classed('right-border', true)
      .append('svg:line')
      .attr('x1', width + 1)
      .attr('x2', width + 1)
      .attr('y1', 0 - (margin.top * 2))
      .attr('y2', height + (margin.top * 0.33));

    // Create the x-axis.
    svg.append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(0, ${height})`)
      .call(xAxis)
      .selectAll('text')
      .attr('y', 6)
      .attr('x', 6)
      .style('text-anchor', 'start');

    // Create the y-axis grid lines.
    svg.append('g')
      .classed('y-grids', true)
      .selectAll('.y-grid')
      .data(() => {
        // Remove the lowest item from the yAxis values.
        const values = yAxisTickValues;

        // If the first value is the lowest value, remove it from the array.
        if (values[0] === (lowestPrice * tickSize)) {
          values.shift();
        }

        // Return the values.
        return values;
      })
      .enter().append('svg:line')
      .attr('class', 'y-grid')
      .attr('y1', (d) => {
        return y(d);
      })
      .attr('x1', width)
      .attr('y2', (d) => {
        return y(d);
      })
      .attr('x2', 0);

    // Create the y-axis.
    svg.append('g')
      .attr('class', 'y-axis')
      .attr('transform', `translate(${width}, 0)`)
      .call(yAxis);

    // Create the bar tails.
    svg.selectAll('.tails')
      .data(bars)
      .enter()
      .append('line')
      .attr('class', (bar: Bar) => {
        return (bar.open > bar.close) ? 'tails down' : 'tails up';
      })
      .attr('x1', (bar: Bar, i: number) => {
        return x(i) + 5;
      }) 
      .attr('x2', (bar: Bar, i: number) => {
        return x(i) + 5;
      }) 
      .attr('y1', (bar: Bar) => {
        return y(bar.high * tickSize);
      })
      .attr('y2', (bar: Bar) => {
        return y(bar.low * tickSize);
      });

    // Create the bar body.
    svg.selectAll('.bar')
      .data(bars)
      .enter()
      .append('rect')
      .attr('class', (bar: Bar) => {
        return (bar.open > bar.close) ? 'bar down' : 'bar up';
      })
      .attr('x', (d, i) => {
        return x(i);
      })
      .attr('width', () => {
        return 10;
        //return xOrdinal.bandwidth();
        //return Math.round((width / bars.length) * 0.6);
      })
      .attr('y', (bar, index) => {
        if (bar.open > bar.close) {
          return y(bar.open * tickSize);
        } else {
          return y(bar.close * tickSize);
        }
      })
      .attr('height', (bar, index) => { 
        if (bar.open > bar.close) {
          return y(bar.close * tickSize) - y(bar.open * tickSize); 
        } else {
          if (bar.open === bar.close) {
            return 2; 
          } else {
            return y(bar.open * tickSize) - y(bar.close * tickSize); 
          }
        }
      });
    
    svg.append('svg:text')
      .attr('class', 'chart-title')
      .attr('x', 0)
      .attr('y', -margin.top)
      .text('Cumulative Delta');

    return el;
  }

  /**
   * Converts chart bars to cumulative delta bars.
   * @method convertChartBarsToDeltaBars
   *
   * @param { Array<Bar> } bars - array of chart bars.
   *
   * @return Array<DeltaBar>
   */
  public static convertChartBarsToDeltaBars(bars: Array<Bar>): Array<DeltaBar> {
    // Declare an empty delta bars array.
    let deltaBars: Array<DeltaBar> = [];

    for (let i = 0; i < bars.length; i++) {
      const bar: DeltaBar = {
        open: bars[i].cumulativeDelta.open,
        high: bars[i].cumulativeDelta.high,
        low: bars[i].cumulativeDelta.low,
        close: bars[i].cumulativeDelta.close,
        lastTransactionDate: bars[i].lastTransactionDate,
        lastRawId: bars[i].lastRawId
      };

      deltaBars.push(bar);
    }

    return deltaBars;
  }
}
