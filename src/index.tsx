import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Chart from './charts/Chart';
import './index.css';

ReactDOM.render(
  <div className="dashboard">
    <div className="charts">
      <Chart 
        instrument="es" 
        chartType="tick" 
        scale="500" 
        displayType="candlestick" 
        title="ES Tick 500" 
        tickSize="0.25" 
      />
      <Chart
        instrument="es" 
        chartType="renko"
        scale="20-10-20"
        displayType="renko"
        title="ES Renko 20-10-20"
        tickSize="0.25"
      />
      <Chart
        instrument="es"
        chartType="renko"
        scale="10-1-10"
        displayType="renko"
        title="ES Renko 10-1-10"
        tickSize="0.25"
      />
      <Chart
        instrument="es"
        chartType="renko"
        scale="40-20-40"
        displayType="renko"
        title="ES Renko 40-20-40"
        tickSize="0.25"
      />
    </div>
  </div>,
  document.getElementById('root') as HTMLElement
);
